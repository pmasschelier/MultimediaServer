# MediaController

Le but de projet est de créer l'ébauche du logiciel d'une set-top box multimédia permettant de jouer de la musique, des vidéos, des films, d'afficher des photos, etc.

## Installation

```
cd cpp
make run
```
Puis dans un autre terminal
```
cd swing
make run
```

## Fonctionnalités du serveur (requêtes acceptées)
- GETALLMEDIA : Renvoie tous les médias de la BDD
- GETALLGROUPS : Renvoie tous les groupes de la BDD
- GETGROUP : Renvoie tous les médias d'un groupe
- GETMEDIA : Renvoie les infos d'un média
- REMOVEMEDIA : Supprime un media
- REMOVEGROUP : Supprime un groupe
- ADDMEDIATOGROUP : Ajoute un media à un groupe
- REMOVEMEDIAFROMGROUP : Supprime un média d'un groupe
- ADDPHOTO : Crée un photo
- ADDVIDEO : Crée une vidéo
- ADDFILM : Crée un film
- ADDGROUP : Crée un groupe
- PLAY : Joue un média

## Fonctionnalités du client
- Choix du serveur et du port de connection dans la barre d'outils
- Connexion et rafraichissement manuels ET automatique (connexion au lancement)
- Ajout de photos, vidéos et films
- Ajout de groupe
- Ajout et suppression de médias aux groupes
- Jouer un média
- Changer le thème de l'interface

## Interface utilisateur

### Application à l'ouverture

![](gitlab/Capture%20d’écran%20du%202022-11-27%2003-38-07.png "Application à l'ouverture")

### Application connectée

![](gitlab/Capture%20d’écran%20du%202022-11-27%2003-39-26.png "Application connectée")

### Ajout d'un film

![](gitlab/Capture%20d’écran%20du%202022-11-27%2004-56-49.png "Ajout d'un film")

## Questions
Toutes les étapes ont été traitées et toutes les questions ont été répondues.

### 4e Etape: Photos et videos
> Comment appelle-t'on ce type de méthode et comment faut-il les déclarer ?

Media::play est une méthode virtuelle pure. Elle est déclarée comme suit :
```cpp
virtual void play() const = 0;
```
> Si vous avez fait ce qui précède comme demandé, il ne sera plus possible d'instancer des objets de la classe de base. Pourquoi ?

Car Media est désormais une classe abstraite (elle possède une méthode virtuel pure) et ne peut donc plus être instanciée.
Sinon l'on pourrait définir des objets dont une méthode n'est pas implémentée.

### 5e Etape: Traitement uniforme (en utilisant le polymorphisme)
> Quelle est la propriété caractéristique de l'orienté objet qui permet de faire cela ?
Le polymorphisme.
> Qu'est-il spécifiquement nécessaire de faire dans le cas du C++ ?

Utiliser des pointeurs et des méthodes virtuelles.
> Quel est le type des éléments du tableau : le tableau doit-il contenir des objets ou des pointeurs vers ces objets ? Pourquoi ? Comparer à Java.

Les éléments du tableau sont des pointeurs sur des medias. Sans cela on ferait du upcasting par exemple de Photo vers Media et la photo perdrait ses propriétés de photo et l'on aurait plus de polymorphise.

### 7e étape. Destruction et copie des objets
> Parmi les classes précédemment écrites quelles sont celles qu'il faut modifier afin qu'il n'y ait pas de fuite mémoire quand on détruit les objets ?

Principalement les Film qui allouent dynamiquement leur tableau de chapitres.

> La copie d'objet peut également poser problème quand ils ont des variables d'instance qui sont des pointeurs.
> Quel est le problème et quelles sont les solutions ?

Si on copie simplement deux objects avec le constructeur de copie par défaut les pointeurs sont copiés mais pas leur contenu.
Deux objets différents possèdent donc deux pointeurs qui pointent vers le même contenu (ici le tableau de chapitres).
Il faut donc redéfinir le constructeur de copie pour copier correctement le contenu pointé par les pointeurs.

### 8e étape. Créer des groupes
> On rappelle aussi que la liste d'objets doit en fait être une liste de pointeurs d'objets. Pourquoi ? Comparer à Java.

Une liste ne peut pas être hétérogène en C++ et on a besoin de pointeurs pour conserver le polymorphisme (cf. 5e étape).
En Java les objets ne peuvent de toute manière être manipulés que sous forme de référence, la question ne se pose donc pas.

### 1ere Etape: Fenêtre principale et quelques interacteurs
> Lancez votre programme, cliquez plusieurs fois sur les deux premiers bouton, retaillez la fenêtre. Que constate-t'on ?

Quand la zone de texte est trop petite une partie du texte est cachée et ne peut plus être vue.

## Icones :

https://icon-icons.com/fr/pack/Media/3630

https://icon-icons.com/fr/pack/Interface/3629
