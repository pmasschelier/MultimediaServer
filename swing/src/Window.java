import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * <b>Classe représentant la fenêtre principale</b>
 * <p>Window hérite de JFrame.</p>
 * <p>La fenêtre contient une MenuBar, une ToolBar, une liste des groupe, une liste des medias dans le groupe séléctionné
 * et une zone de text affichant les informations sur le media séléctionné.</p>
 * 
 * @see MediaOptionChooser
 * @see ThemesMenuItem
 * @see Client
 * 
 * @author masschelier@telecom-paris.fr
 *
 */
/**
 * @author tintin
 *
 */
/**
 * @author tintin
 *
 */
final public class Window extends JFrame implements ListSelectionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JMenuBar menuBar;
	private JMenu fileMenu, editMenu, groupMenu;
	private JToolBar toolBar;
	private JButton button1, button2, buttonPlay, editGroupsButton;
	private JScrollPane scrollpane;
	private JTextField hostText, portText;
	JPanel centerPanel;
	JList<String> medialist, groupslist;
	private JTextArea textarea;
	
	private Client client = null;

	public static String iconsdir = "ressources/icons/";
	static final String DEFAULT_HOST = "localhost";
	static final int DEFAULT_PORT = 3331;
	
	public Window() {
		super("Media Controller");
		
		Window that = this; // To have a reference to this in anonymous classes
		setMinimumSize(new Dimension(750, 300));
		setPreferredSize(new Dimension(800, 500));
		
		add(centerPanel = new JPanel(new GridBagLayout()), BorderLayout.CENTER);
		centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 2;
		c.gridheight = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 0.0;
		centerPanel.add(new JLabel("Groups : "), c);
		c.gridy = 1;
		c.weighty = 1.0;
		centerPanel.add(new JScrollPane(groupslist = new JList<String>()), c);
		groupslist.addListSelectionListener(this);
		c.gridy = 2;
		c.weighty = 0.0;
		centerPanel.add(new JLabel("Medias : "), c);
		c.gridy = 3;
		c.weighty = 1.0;
		centerPanel.add(new JScrollPane(medialist = new JList<String>()), c);
		medialist.addListSelectionListener(this);
		c.gridx = 2;
		c.gridy = 1;
		c.gridheight = 4;
		centerPanel.add(scrollpane = new JScrollPane(textarea = new JTextArea(20, 30)), c);
		textarea.setEditable(false);
		textarea.setMargin(new Insets(5, 5, 5, 5));
		
		AbstractAction addAction = new AbstractAction("Add", new ImageIcon(iconsdir + "multimedia_add_create_music_icon_229135.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { addMedia(); }
		};
		addAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Add a new media to the database");
		addAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_N);
		AbstractAction photoAction = new AbstractAction("Add a picture", new ImageIcon(iconsdir + "camera_image_icon_227427.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { new MediaOptionChooser(that, MediaOptionChooser.PHOTO); }
		};
		photoAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Add a new picture to the database");
		AbstractAction videoAction = new AbstractAction("Add a video", new ImageIcon(iconsdir + "video_camera_icon_227424.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { new MediaOptionChooser(that, MediaOptionChooser.VIDEO); }
		};
		videoAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Add a new video to the database");
		AbstractAction movieAction = new AbstractAction("Add a movie", new ImageIcon(iconsdir + "play_circle_icon_227420.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { new MediaOptionChooser(that, MediaOptionChooser.FILM); }
		};
		movieAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Add a new movie to the database");
		AbstractAction removeAction = new AbstractAction("Remove", new ImageIcon(iconsdir + "close_delete_remove_music_icon_229134.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { removeMedia(); }
		};
		removeAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Remove the selected media");
		removeAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_DELETE);
		AbstractAction refreshAction = new AbstractAction("Refresh media list", new ImageIcon(iconsdir + "synchronize_icon_227376.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { updateMediaList(); updateGroupsList(); }
		};
		refreshAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Refresh the whole media list");
		refreshAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_R);
		AbstractAction playAction = new AbstractAction("Play", new ImageIcon(iconsdir + "player_play_icon_227418.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { playMedia(); }
		};
		playAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Play the selected media");
		playAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_P);
		AbstractAction quitAction = new AbstractAction("Exit", new ImageIcon(iconsdir + "exit_icon_227388.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { System.exit(0); }
		};
		quitAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_Q);
		AbstractAction connectAction = new AbstractAction("Connect", new ImageIcon(iconsdir + "swap_icon_227393.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { connect(); }
		};
		connectAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_C);
		connectAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Connect to the selected host and port");
		AbstractAction createGroupAction = new AbstractAction("Create group", new ImageIcon(iconsdir + "playlist_icon_227431.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { addGroup(); }
		};
		createGroupAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Create a new group of medias");
		AbstractAction removeGroupAction = new AbstractAction("Remove group", new ImageIcon(iconsdir + "close_icon_227389.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { removeGroup(); }
		};
		removeGroupAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Remove a group of medias (doesn't remove the content)");
		AbstractAction editGroupsAction = new AbstractAction("Add to group", new ImageIcon(iconsdir + "product_box_icon_225732.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { editGroups(); }
		};
		removeGroupAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Add the selected media to a group");
		/*AbstractAction createGroupAction = new AbstractAction("Create group", new ImageIcon(iconsdir + "playlist_icon_227431.png")) {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) { addGroup(); }
		};
		connectAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Create a new group of medias");*/
		
		
		setJMenuBar(menuBar = new JMenuBar());
		fileMenu = new JMenu("Media");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.add(connectAction);
		fileMenu.add(refreshAction);
		fileMenu.addSeparator();
		fileMenu.add(quitAction);
		menuBar.add(fileMenu);
		
		editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		editMenu.add(photoAction);
		editMenu.add(videoAction);
		editMenu.add(movieAction);
		editMenu.addSeparator();
		editMenu.add(removeAction);
		editMenu.addSeparator();
		editMenu.add(playAction);
		menuBar.add(editMenu);
		
		groupMenu = new JMenu("Groups");
		groupMenu.setMnemonic(KeyEvent.VK_G);
		groupMenu.add(createGroupAction);
		groupMenu.add(removeGroupAction);
		menuBar.add(groupMenu);
		
		menuBar.add(new ThemesMenuItem(this));
		
		add(toolBar = new JToolBar(), BorderLayout.NORTH);		
		toolBar.add(photoAction);
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(videoAction);
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(movieAction);
		toolBar.addSeparator(new Dimension(20, 20));
		toolBar.add(removeAction);
		toolBar.addSeparator(new Dimension(20, 20));
		toolBar.add(playAction);
		toolBar.add(Box.createHorizontalGlue());
		toolBar.add(new JLabel("Host : "));
		hostText = new JTextField(DEFAULT_HOST, 10);
		Dimension size = hostText.getMaximumSize();
		hostText.setMaximumSize(new Dimension(100, size.height));
		hostText.setMargin(new Insets(0, 10, 0, 0));
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(hostText);
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(new JLabel("Port : "));
		portText = new JTextField(Integer.toString(DEFAULT_PORT), 10);
		portText.setMaximumSize(new Dimension(100, size.height));
		portText.setMargin(new Insets(0, 10, 0, 0));
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(portText);
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(connectAction);
		toolBar.addSeparator(new Dimension(10, 10));
		toolBar.add(refreshAction);
		
		//JPanel buttonPanel = new JPanel();
		button1 = new JButton();
		button1.setAction(addAction);
		button2 = new JButton();
		button2.setAction(removeAction);
		buttonPlay = new JButton();
		buttonPlay.setAction(playAction);
		editGroupsButton = new JButton();
		editGroupsButton.setAction(editGroupsAction);
		c.gridwidth = 1;
		c.weighty = 0.;
		c.gridy = 4;
		c.gridx = 0;
		centerPanel.add(button1, c);
		c.gridx = 1;
		centerPanel.add(button2, c);
		c.gridx = 2;
		centerPanel.add(editGroupsButton, c);
		c.gridx = 3;
		centerPanel.add(buttonPlay, c);
		//add(buttonPanel, BorderLayout.SOUTH);
		buttonPlay.setEnabled(false);
		editGroupsButton.setEnabled(false);
		button2.setEnabled(false);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		pack();
		setVisible(true);
		
		int res = JOptionPane.showConfirmDialog(this , "Do you want to connect to default server : " + DEFAULT_HOST + ":" + DEFAULT_PORT + " ?", "Default server ?",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(res == JOptionPane.YES_OPTION) {
			connect();
		}
	}
	
	/**
	 * Affiche une fenêtre d'erreur
	 * @param title Titre de la fenêtre d'erreur
	 * @param msg Message à afficher
	 */
	public void showError(String title, String msg) {
		JOptionPane.showMessageDialog(this, msg, title, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Renvoie le nom du media séléctionné
	 * @return Nom du media
	 */
	public String getSelectedMediaName() {
		return medialist.getSelectedValue();
	}
	
	/** Test si le groupe séléctionné est celui contenant tous les médias
	 * @return true ssi le groupe séléctionné est celui contenant tous les médias
	 */
	public boolean allMediaSelected() {
		return groupslist.getSelectedIndex() == 0;
	}
	
	/** Renvoie le nom du groupe séléctionné
	 * @return Nom du groupe
	 */
	public String getSelectedGroupName() {
		return groupslist.getSelectedValue();
	}
	
	/**
	 * Crée un client selon les informations données dans la toolbar.
	 */
	public void connect() {
		String host = hostText.getText();
		int port = 0;
		try {
			port = Integer.parseInt(portText.getText());
			client = new Client(host, port);
			updateMediaList();
			updateGroupsList();
		}  
		catch (NumberFormatException e1) {
			JOptionPane.showMessageDialog(this, "Incorrect port number", "Connection error", JOptionPane.ERROR_MESSAGE);
		} catch (UnknownHostException e1) {
			JOptionPane.showMessageDialog(this, "Couldn't find host " + host + ":" + port, "Connection error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(this, "Couldn't reach host " + host + ":" + port, "Connection error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Envoie une commande au serveur
	 * @param command Commande à envoyer
	 * @return Réponse du serveur
	 */
	public String sendCommand(String command) {
		if(client == null) {
			JOptionPane.showMessageDialog(this, "Client is not connected to a remote server", "Not connected", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		return client.send(command);
	}

	/**
	 * Envoie une commande au serveur pour mettre à jour la liste des medias
	 * selon le nom du groupe séléctionné
	 */
	public void updateMediaList() {
		String[] data = null;
		if(allMediaSelected())
			data = sendCommand("GETALLMEDIA").split(";");
		else
			data = sendCommand("GETGROUP " + groupslist.getSelectedValue()).split(";");
		medialist.setListData(data);
	}
	
	/**
	  * Envoie une commande au serveur pour mettre à jour la liste des groupes
	  * et ajoute un groupe "All medias" en premier.
	 */
	public void updateGroupsList() {
		String[] groups = sendCommand("GETALLGROUPS").split(";");
		String[] allgroups = new String[groups.length + 1];
		allgroups[0] = "All medias";
		System.arraycopy(groups, 0, allgroups, 1, groups.length);
		groupslist.setListData(allgroups);
		if(groupslist.isSelectionEmpty())
			groupslist.setSelectedIndex(0);
	}
	
	/**
	 * Vérifie qu'un media est bien séléctionné
	 * @return true ssi un media est séléctionné
	 */
	public boolean testSelected() {
		if(medialist.isSelectionEmpty()) {
			JOptionPane.showMessageDialog(this, "No media selected", "Selection empty", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Vérifie qu'un groupe est bien séléctionné
	 * @return true ssi un groupe est séléctionné
	 */
	public boolean testSelectedGroup() {
		if(groupslist.isSelectionEmpty()) {
			JOptionPane.showMessageDialog(this, "No group selected", "Selection empty", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Supprime le media séléctionné (appelle testSelected) si un groupe est séléctionné
	 * demande aussi si on veut supprimer le media de ce groupe uniquement ou bien de la BDD toute entière.
	 * Si "All medias" est séléctionné demande une confirmation avant de supprimer le media de la BDD.
	 */
	public void removeMedia() {
		if(testSelected() ) {
			int res;
			if(allMediaSelected()) {
				res = JOptionPane.showConfirmDialog(this, "Do you really want to delete this media ?", "Are you sure ?", JOptionPane.YES_NO_OPTION);
				if(res == JOptionPane.NO_OPTION) return;
			}
			else {
				res = JOptionPane.showConfirmDialog(this, "Do you want to remove this media from the database too ?", "Remove from database ?", JOptionPane.YES_NO_CANCEL_OPTION);
				if(res == JOptionPane.CANCEL_OPTION)
					return;
				sendCommand("REMOVEMEDIAFROMGROUP " + groupslist.getSelectedValue() + ";" + medialist.getSelectedValue());
			}
			if(res == JOptionPane.YES_OPTION)
				sendCommand("REMOVEMEDIA " + medialist.getSelectedValue());
			updateMediaList();
		}
	}

	/**
	 * Envoie une commande pour jouer le media séléctionné
	 */
	public void playMedia() {
		if(testSelected())
			sendCommand("PLAY " + medialist.getSelectedValue());
	}
	
	/**
	 * Ouvre une boite de dialogue pour créer un nouveau media.
	 * Si un groupe est séléctionné, propose de l'ajouter directement à ce groupe.
	 * @see MediaOptionChooser
	 */
	public void addMedia() {
		JDialog mediatype = new JDialog(this, "Choose media type");
		mediatype.setPreferredSize(new Dimension(250, 110));
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.LINE_AXIS));
		centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 40));
		mediatype.add(centerPanel);
		JComboBox<String> list = new JComboBox<String>();
		list.addItem("Photo");
		list.addItem("Video");
		list.addItem("Film");
		centerPanel.add(list);
		
		JPanel buttonPane = new JPanel();
		JButton cancelButton = new JButton("Cancel");
		JButton okButton = new JButton("OK");
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(cancelButton);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(okButton);
		
		mediatype.add(buttonPane, BorderLayout.SOUTH);
		
		cancelButton.addActionListener(e -> mediatype.dispose());
		
		okButton.addActionListener(e -> {
			mediatype.dispose();
			new MediaOptionChooser(this, list.getSelectedIndex());
		});
		
		mediatype.setModal(true);
		mediatype.pack();
		mediatype.setVisible(true);			
	}
	
	/**
	 * Ajoute un groupe
	 */
	public void addGroup() {
		String name = JOptionPane.showInputDialog(this, "Group's name : ");
		sendCommand("ADDGROUP " + name);
		updateGroupsList();
	}
	
	/**
	 * Supprime le groupe séléctionné si ce n'est pas "All médias"
	 */
	public void removeGroup() {
		if(allMediaSelected())
			JOptionPane.showMessageDialog(this, "You can't delete this group", "Can't delete", JOptionPane.ERROR_MESSAGE);
		else if(testSelectedGroup()
				&& JOptionPane.showConfirmDialog(this, "Do you really want to delete this group ?", "Are you sure ?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			sendCommand("REMOVEGROUP " + groupslist.getSelectedValue());
			updateGroupsList();
		}
	}
	
	/**
	 * Ajoute le media séléctionné au groupe choisi dans la boite de dialogue
	 */
	public void editGroups() {
		JDialog choosegroup = new JDialog(this, "Choose a group");
		choosegroup.setPreferredSize(new Dimension(250, 110));
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.LINE_AXIS));
		centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 40));
		choosegroup.add(centerPanel);
		JComboBox<String> list = new JComboBox<String>();
		for(int i = 1; i < groupslist.getModel().getSize(); i++)
			list.addItem(groupslist.getModel().getElementAt(i));
		centerPanel.add(list);
		
		JPanel buttonPane = new JPanel();
		JButton cancelButton = new JButton("Cancel");
		JButton okButton = new JButton("OK");
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(cancelButton);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(okButton);
		
		choosegroup.add(buttonPane, BorderLayout.SOUTH);
		
		cancelButton.addActionListener(e -> choosegroup.dispose());
		
		okButton.addActionListener(e -> {
			choosegroup.dispose();
			sendCommand("ADDMEDIATOGROUP " + list.getSelectedItem() + ";" + medialist.getSelectedValue());
		});
		
		choosegroup.setModal(true);
		choosegroup.pack();
		choosegroup.setVisible(true);			
	}

	/**
	 * Listener pour les listes de medias et de groupe
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(e.getSource() == medialist) {
			String text = "";
			boolean selected = ! medialist.isSelectionEmpty();
			if(selected)
				text = sendCommand("GETMEDIA " + medialist.getSelectedValue()).replace(';', '\n');
			textarea.setText(text);
			buttonPlay.setEnabled(selected);
			editGroupsButton.setEnabled(selected);
			button2.setEnabled(selected);
		}
		else if(e.getSource() == groupslist)
			updateMediaList();
	}
}
