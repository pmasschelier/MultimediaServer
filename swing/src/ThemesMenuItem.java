import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.* ;

/**
 * <b>Classe représentant le sous-menu permettant de choisir le thème de la fenêtre</b>
 * <p>Le menu est généré automatiquement à partir des thèmes proposés par
 * UIManager.getInstalledLookAndFeels()</p>
 * 
 * @see Window
 *
 * @author masschelier@telecom-paris.fr
 *
 */
final public class ThemesMenuItem extends JMenu {
	
	final public class ChangeThemeAction implements ActionListener {
		final UIManager.LookAndFeelInfo info;
		final Window app;
		
		public ChangeThemeAction(UIManager.LookAndFeelInfo info, Window app) {
			this.info = info;
			this.app = app;
		}
		
		public void actionPerformed(ActionEvent e) {
			try {
				UIManager.setLookAndFeel(info.getClassName());
				SwingUtilities.updateComponentTreeUI(app);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e1) {
				app.showError("Unsupported theme", e1.getMessage());
			}
		}
	}
	
	private static final long serialVersionUID = 5576535944802200870L;

	ThemesMenuItem(Window app) {
		super("Thème");
		
		ButtonGroup group = new ButtonGroup();
		
		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		for(UIManager.LookAndFeelInfo look : looks) {
			if(look.getClassName().equals("com.sun.java.swing.plaf.gtk.GTKLookAndFeel")) // Ce thème fait crasher l'app
				continue;
			
			JRadioButtonMenuItem menuitem = new JRadioButtonMenuItem(look.getName());
			menuitem.addActionListener(new ChangeThemeAction(look, app));
			group.add(menuitem);
			add(menuitem);
			
			if(look.getClassName().equals(UIManager.getCrossPlatformLookAndFeelClassName()))
				menuitem.setSelected(true);
		}
	}
	
	
}

