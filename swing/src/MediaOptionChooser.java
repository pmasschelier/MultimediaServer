import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * <b>Classe représentant la boite de dialogue de crétion de média.</b>
 * <p>MediaOptionChooser hérite de JDialog.</p>
 * 
 * @see Window
 * 
 * @author masschelier@telecom-paris.fr
 *
 */
/**
 * @author tintin
 *
 */
/**
 * @author tintin
 *
 */
/**
 * @author tintin
 *
 */
public class MediaOptionChooser extends JDialog implements ListSelectionListener{

	private static final long serialVersionUID = 1L;
	
	GridBagConstraints c;
	JComboBox<String> mediatype;
	JLabel label1, label2, label3, label4, label5, label6;
	JTextField nameField, locationField;
	JPanel formPanel, centerPanel, updatingPanel;
	JPanel photoPanel, videoPanel, filmPanel;
	JButton cancelButton, okButton;
	JSpinner width, height, duration;
	ArrayList<Integer> chapdurations;
	JList<String> chapters;
	JButton addButton, removeButton, editButton;
	JSpinner chapduration;
	
	public static final int PHOTO = 0;
	public static final int VIDEO = 1;
	public static final int FILM = 2;
	
	Window parent;
	int type = -1;

	/**
	 * @param parent Fenetre mère
	 * @param type Type de media à creer : PHOTO, VIDEO, FILM
	 */
	public MediaOptionChooser(Window parent, int type) {
		super(parent, "Media data");
		this.parent = parent;
		this.type = type;
		
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		
		formPanel = new JPanel();
		formPanel.setLayout(new GridBagLayout());
		add(formPanel);

		c = new GridBagConstraints();
		c.insets = new Insets(5, 10, 5, 10);

		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 2; c.gridheight = 1;
		c.anchor = GridBagConstraints.LINE_END;
		
		c.gridy = 0;
		formPanel.add(label2 = new JLabel("Nom du fichier :"), c);
		c.gridy = 1;
		formPanel.add(label3 = new JLabel("Emplacement sur le disque :"), c);
		
		c.gridx = 2;
		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 0;
		formPanel.add(nameField = new JTextField(), c);
		nameField.setColumns(30);
		c.gridy = 1;
		formPanel.add(locationField = new JTextField(), c);
		
		switch(type) {
		case PHOTO:
			setPhotoLayout();
			break;
		case VIDEO:
			setVideoLayout();
			break;
		case FILM:
			setFilmLayout();
			break;
		}
		
		JPanel buttonPanel = new JPanel();
		BoxLayout buttonLayout = new BoxLayout(buttonPanel, BoxLayout.X_AXIS);
		buttonPanel.setLayout(buttonLayout);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(cancelButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 1)));
		buttonPanel.add(okButton, c);
		
		okButton.addActionListener(e -> addMedia());
		cancelButton.addActionListener(e -> dispose());
		
		add(buttonPanel, BorderLayout.SOUTH);
		
		setModal(true);
		pack();
		setVisible(true);
	}
	
	/**
	 * Crée le layout pour créer une photo
	 */
	private void setPhotoLayout() {
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.LINE_END;
		
		formPanel.add(label4 =  new JLabel("Width : "), c);
		c.gridy = 3;
		formPanel.add(label5 = new JLabel("Height : "), c);
		
		c.gridx = 2;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		
		width = new JSpinner(new SpinnerNumberModel(1024, 0, null, 1));
		c.gridy = 2;
		formPanel.add(width, c);
		height = new JSpinner(new SpinnerNumberModel(800, 0, null, 1));
		c.gridy = 3;
		formPanel.add(height, c);
	}
	
	/**
	 * Crée le layout pour créer une vidéo
	 */
	private void setVideoLayout() {
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.LINE_END;
		formPanel.add(label4 =  new JLabel("Duration : "), c);
		duration = new JSpinner(new SpinnerNumberModel(60, 0, null, 1));
		c.gridx = 2;
		c.gridwidth = 1;
		c.gridy = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		formPanel.add(duration, c);
	}
	
	/**
	 * Crée le layout pour créer un film
	 */
	private void setFilmLayout() {
		setVideoLayout();
		
		chapdurations = new ArrayList<>();
		
		JPanel chapPanel = new JPanel();
		chapPanel.setLayout(new BorderLayout());
		chapPanel.setBorder(BorderFactory.createTitledBorder("Chapters' duration"));
		
		JPanel borderPanel = new JPanel();
		borderPanel.setLayout(new BoxLayout(borderPanel, BoxLayout.PAGE_AXIS));
		borderPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 5, 20));
		chapPanel.add(borderPanel);
		
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 4;
		c.gridheight = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		formPanel.add(chapPanel, c);
		
		chapters = new JList<>();
		JScrollPane scroll = new JScrollPane(chapters);
		scroll.setPreferredSize(new Dimension(100, 80));
		scroll.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		borderPanel.add(scroll);
		
		borderPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(new JLabel("duration : "));
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 0)));
		chapduration = new JSpinner(new SpinnerNumberModel(10, 0, null, 1));
		buttonPanel.add(chapduration);
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 0)));
		addButton = new JButton("+");
		addButton.setMargin(new Insets(2, 20, 2, 20));
		buttonPanel.add(addButton, c);
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 0)));
		removeButton = new JButton("-");
		removeButton.setEnabled(false);
		removeButton.setMargin(new Insets(2, 20, 2, 20));
		buttonPanel.add(removeButton, c);
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 0)));
		editButton = new JButton("Edit");
		editButton.setEnabled(false);
		editButton.setMargin(new Insets(2, 20, 2, 20));
		buttonPanel.add(editButton, c);
		
		borderPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		addButton.addActionListener(e -> {
			chapdurations.add((Integer) chapduration.getValue());
			updateChaptersList();
		});
		removeButton.addActionListener(e -> {
			chapdurations.remove(chapters.getSelectedIndex());
			updateChaptersList();
		});
		editButton.addActionListener(e -> {
			chapdurations.set(chapters.getSelectedIndex(), (Integer)chapduration.getValue());
			updateChaptersList();
		});
		
		chapters.addListSelectionListener(this);
	}
	
	/**
	 * Demande à l'utilisateur si le media créé doit être ajouté au groupe courant
	 * @return Réponse de l'utilisateur : YES_OPTION, NO_OPTION, CANCEL_OPTION
	 */
	public int askToAddToGroup() {
		int res = JOptionPane.NO_OPTION;
		if(!parent.allMediaSelected())
			 res = JOptionPane.showConfirmDialog(this, "Do you want to add the media to the current group ?", "Add to group ?", JOptionPane.YES_NO_CANCEL_OPTION);
		return res;
	}
	
	/**
	 * Ajoute le media à la BDD et demande à l'utilisateur s'il veut l'ajouter au groupe courant
	 */
	public void addMedia() {
		int res = askToAddToGroup();
		if(res == JOptionPane.CANCEL_OPTION)
			return;
		String command = null;
		switch(type) {
		case PHOTO: 
			command = "ADDPHOTO " + nameField.getText() + ";" + locationField.getText() + ";" + width.getValue() + " " + height.getValue();
			break;
		case VIDEO:
			command = "ADDVIDEO " + nameField.getText() + ";" + locationField.getText() + ";" + duration.getValue();
			break;
		case FILM:
			command = "ADDFILM " + nameField.getText() + ";" + locationField.getText() + ";" + duration.getValue() + String.join(" ", chapdurations.stream().map(i -> i.toString()).collect(Collectors.toList()));
			break;
		}
		parent.sendCommand(command);
		dispose();
		if(res == JOptionPane.YES_OPTION)
			parent.sendCommand("ADDMEDIATOGROUP " + parent.getSelectedGroupName() + ";" + nameField.getText());
		parent.updateMediaList();
	}
	
	/**
	 * Met à jour la liste des chapitres d'un film
	 */
	public void updateChaptersList() {
		String[] result = new String[chapdurations.size()];
		for(int i = 0; i < chapdurations.size(); i++)
			result[i] = "chapter " + (i+1) + " : " + chapdurations.get(i);
		chapters.setListData(result);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(e.getSource() == chapters) {
			if(! chapters.isSelectionEmpty()) {
				editButton.setEnabled(true);
				removeButton.setEnabled(true);
				chapduration.setValue(chapdurations.get(chapters.getSelectedIndex()));
			}
			else {
				editButton.setEnabled(false);
				removeButton.setEnabled(false);
			}
		}
	}
}
