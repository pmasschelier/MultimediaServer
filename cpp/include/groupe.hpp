#ifndef GROUPE_HPP
#define GROUPE_HPP

#include <list>
#include <memory>
#include "media.hpp"

class Groupe;
using GroupePtr = std::shared_ptr<Groupe>;

/**
 * @brief Groupe de média possédant un nom
 */
class Groupe : public std::list<MediaPtr>{

public:

    /**
     * @brief Constructeur de Groupe
     * @param name nom du groupe
	 * @throw std::invalid_argument si le nom contient des caractères dont le code ascii n'est pas compris entre ' ' et 'z'
     */
    explicit Groupe(std::string const& name);

    /**
     * @brief Renvoie le nom du groupe
     * @return Nom du groupe
     */
    std::string getName() const { return name; }

    /**
     * @brief Affiche le nom du groupe puis chaque élément du groupe sur le flux
     * @param os Flux sur lequel les informations vont être affichées.
     */
    void print(std::ostream & os) const;

	void* operator new(std::size_t) = delete;

private:

    std::string name;

};

std::ostream& operator<<(std::ostream & os, Groupe const & group);

#endif