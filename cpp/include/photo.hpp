#ifndef PHOTO_HPP
#define PHOTO_HPP

#include <sstream>

#include "media.hpp"

/**
 * @brief Classe représentant les photos ie. les médias avec une dimension
 */
class Photo : public Media {

public:

    /**
     * @brief Constructeur par défaut de Photo
     */
    Photo() {}

    /**
     * @brief Constructeur de Photo
     * @param name Nom de la photo
     * @param pathname Chemin sur le disque
     * @param width Largeur de la photo
     * @param height Hauteur de la photo
     */
    explicit Photo(std::string const& name, std::string const& pathname, unsigned width, unsigned height) :
        Media(name, pathname), width(width), height(height) {}
    
    /**
     * @brief Destructeur de Photo
     */
    ~ Photo() {}

    /**
     * @brief Renvoie la largeur de la photo
     * @return Largeur de la photo
     */
    unsigned getWidth() const { return width; }

    /**
     * @brief Met à jour la largeur de la photo
     * @param width Nouvelle largeur de la photo
     */
    void setWidth(unsigned width) { this->width = width; }

    /**
     * @brief Renvoie la hauteur de la photo
     * @return Hauteur de la photo
     */
    unsigned getHeight() const { return height; }

    /**
     * @brief Met à jour la hauteur de la photo
     * @param width Nouvelle hauteur de la photo
     */
    void setHeight(unsigned height) { this->height = height; }

    /**
     * @brief Affiche les informations relatives à la photo
     * @param os Flux sur lequel les informations vont être affichées.
     */
    void print(std::ostream & os) const override;

	/**
	 * @brief Serialise la photo
	 * @param os Flux sur lequel la photo va être sérialisé
	 */
	virtual void serialize(std::ostream & os) const override;

	/**
	 * @brief Déserialise la photo
	 * @param is flux depuis lequel la photo doit-être extrait
	 */
	void deserialize(std::istream& is) override;

	/**
	 * @brief Renvoie le nom de la classe
	 * @return "Photo"
	 */
	virtual const std::string getClassName() const override { return "Photo"; }
    
    /**
     * @brief Affiche la photo
     */
    void play() const override;

private:

    unsigned width{}, height{};

};

#endif