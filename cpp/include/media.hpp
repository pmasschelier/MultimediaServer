#ifndef MEDIA_HPP
#define MEDIA_HPP

#include <algorithm>
#include <iostream>
#include <memory>

class Media;
using MediaPtr = std::shared_ptr<Media>;

static inline bool checkName(std::string const& name) {
	return std::all_of(name.begin(), name.end(), [] (char c) { return c >= ' ' && c <= 'z'; });
}

/**
 * @brief Classe abstraite représentant un média
 */
class Media {

public:

    /**
     * @brief Constructeur par défaut de Media
     */
    Media() {};

    /**
     * @brief Constructeur de Media
     * @param name Nom du fichier multimedia
     * @param pathname Chemin vers le fichier multimedia sur le disque
     */
    Media(std::string const& name, std::string const& pathname);

    /**
     * @brief Destructeur virtuel de Media
     */
    virtual ~ Media() {};

    /**
     * @brief Renvoie le nom du média
     * @return Nom du fichier multimedia
     */
    std::string getName() const { return name; }

    /**
     * @brief Met à jour le nom du média
     * @param name Nouveau nom du fichier multimedia
     */
    void setName(std::string const& name);

    /**
     * @brief Renvoie le chemin vers le média
     * @return Chemin vers le fichier multimedia
     */
    std::string getPathname() const { return pathname; }

    /**
     * @brief Met à jour le chemin vers le média
     * @param pathname Nouveau chemin vers le fichier multimedia
     */
    void setPathname(std::string const& pathname);

    /**
     * @brief Affiche les informations relatives au média
     * @param os Flux sur lequel les informations vont être affichées.
     */
    virtual void print(std::ostream & os) const;

	/**
	 * @brief Sérialise le média
	 * @param os Flux sur lequel le média va être sérialisé
	 */
	virtual void serialize(std::ostream & os) const;

	/**
	 * @brief Déserialise le média
	 * @param is Flux depuis lequel le média doit-être extrait
	 */
	virtual void deserialize(std::istream& is);

	/**
	 * @brief Renvoie le nom de la classe
	 * @return "Media"
	 */
	virtual const std::string getClassName() const { return "Media"; }

    /**
     * @brief Joue le fichier multimédia
     */
    virtual void play() const = 0;

	void* operator new(std::size_t) = delete;

private:

    std::string name, pathname;
    
};

/**
 * @brief Opérateur de flux permettant de sérialiser un média
 * @param os Flux sur lequel le média va être sérialisé
 * @param media Media à sérialiser
 * @return os
 */
std::ostream& operator<<(std::ostream & os, Media const & media);

/**
 * @brief Opérateur de flux permettant de désérialiser un média
 * @param is Flux depuis lequel le média va être extrait
 * @param media Media à extraire
 * @return is
 */
std::istream& operator>>(std::istream & is, Media& media);

#endif