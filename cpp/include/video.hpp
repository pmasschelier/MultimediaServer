#ifndef VIDEO_HPP
#define VIDEO_HPP

#include "media.hpp"


/**
 * @brief Classe représentant les vidéos ie. les médias avec une durée
 */
class Video : public Media {

public:

    /**
     * @brief Constructeur par défaut de Vdeo
     */
    Video() {}

    /**
     * @brief Constructeur de Video
     * @param name Nom de la vidéo
     * @param pathname Chemin sur le disque
     * @param duration Durée de la vidéo
     */
    Video(std::string const& name, std::string const& pathname, unsigned duration) :
        Media(name, pathname), duration(duration) {}
    
    /**
     * @brief Destructeur virtuel de Video
     */
    virtual ~ Video() {}

    /**
     * @brief Renvoie la durée de la vidéo
     * @return Durée de la vidéo
     */
    unsigned getDuration() const { return duration; }

    /**
     * @brief Met à jour la durée de la vidéo
     * @param duration Nouvelle durée de la vidéo
     */
    void setDuration(unsigned duration) { this->duration = duration; }

    /**
     * @brief Affiche les informations relatives à la vidéo
     * @param os Flux sur lequel les informations vont être affichées.
     */
    void print(std::ostream & os) const override;

	/**
	 * @brief Serialise la vidéo
	 * @param os Flux sur lequel la vidéo va être sérialisé
	 */
	virtual void serialize(std::ostream & os) const override;

	/**
	 * @brief Déserialise la vidéo
	 * @param is flux depuis lequel la vidéo doit-être extrait
	 */
	void deserialize(std::istream& is) override;

	/**
	 * @brief Renvoie le nom de la classe
	 * @return "Video"
	 */
	virtual const std::string getClassName() const override { return "Video"; }
    
    /**
     * @brief Affiche la vidéo
     */
    void play() const override;

private:

    unsigned duration{};
    
};

#endif