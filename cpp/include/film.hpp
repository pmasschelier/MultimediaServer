#ifndef FILM_HPP
#define FILM_HPP

#include "video.hpp"

/**
 * @brief Classe représentant les films ie. les vidéos avec des chapitres.
 */
class Film : public Video {

public:

    /**
     * @brief Constructeur par défaut de Film
     */
    Film() {}

    /**
     * @brief Constructeur de Film
     * @param name Nom du film
     * @param pathname Chemin sur le disque
     * @param duration Durée du film
     * @param nb_chapters Nombre de chapitre (taille du tableau passé par chapters)
     * @param chapters Tableau d'entiers indiquant la durée de chaque chapitre
	 * @warning Le tableau passé par chapters est copié, les modification de chapters n'affecterons pas le tableau conservé par l'objet.
     */
    Film(std::string const& name, std::string const& pathname, unsigned duration, unsigned nb_chapters = 0, unsigned const * const chapters = nullptr);

    /**
     * @brief Constructeur de copie
     * @param other film à copier
     */
    Film(Film const& other);

    /**
     * @brief Opérateur de copir
     * @param other film à copier
     * @return film copié
     */
    Film& operator=(Film const& other);

    /**
     * @brief Destructeur virtuel de Film
     */
    virtual ~ Film();

    /**
     * @brief Modifie la table des chapitre du film
     * @param nb_chapters nombre de chapitre (taille du tableau passé par chapters)
     * @param chapters tableau d'entiers indiquant la durée de chaque chapitre
	 * @warning Le tableau passé par chapters est copié, les modification de chapters n'affecterons pas le tableau conservé par l'objet.
     */
    void setChapters(unsigned nb_chapters, unsigned const * const chapters);

    /**
     * @brief Renvoie le nombre de chapitres du film
     * @return nombre de chapitres du film
	 * @see Film::getChapters
     */
    unsigned getNbChapters() const { return nb_chapters; }

    /**
     * @brief Renvoie la table des chapitres du film
     * @return tableau d'entiers indiquant la durée de chaque chapitre
	 * @warning le tableau n'est pas copié mais il est constant et ne peut donc pas être modifié sans être copié par l'utilisateur.
     */
    unsigned const * getChapters() const;

    /**
     * @brief Affiche les informations relatives au film
     * @param os Flux sur lequel les informations vont être affichées.
     */
    void print(std::ostream & os) const override;

	void serialize(std::ostream & os) const;

	void deserialize(std::istream& is);

	/**
	 * @brief Renvoie le nom de la classe
	 * @return "Film"
	 */
	virtual const std::string getClassName() const override { return "Film"; }

private:

    unsigned* m_chapters = nullptr;
    unsigned nb_chapters{};

};

#endif