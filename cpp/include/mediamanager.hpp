#ifndef MEDIAMANAGER_HPP
#define MEDIAMANAGER_HPP

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "groupe.hpp"
#include "photo.hpp"
#include "film.hpp"

enum ERROR_CODE {
	SUCCESS = 200,
	COMMAND_ERROR = 400,
	ARGUMENTS_ERROR = 401,
};

/**
 * @brief Gestionnaire de médias et de groupes de médias
 */
class MediaManager {

public:

	/**
	 * @brief Constructeur par défaut de MediaManager
	 */
	MediaManager() {}

	/**
	 * @brief Crée une photo et l'ajoute au dictionnaire des médias
	 * @param name Nom de la photo
	 * @param pathname Chemin vers le fichier sur le disque
	 * @param width Largeur de la photo
	 * @param height Hauteur de la photo
	 * @return Pointeur sur la photo (std::shared_ptr)
	 */
	MediaPtr createPhoto(std::string const& name, std::string const& pathname, unsigned width, unsigned height);

	/**
	 * @brief Crée une vidéo et l'ajoute au dictionnaire des médias
	 * @param name Nom de la vidéo
	 * @param pathname Chemin vers le fichier sur le disque
	 * @param duration Durée de la vidéo
	 * @return Pointeur sur la vidéo (std::shared_ptr)
	 */
	MediaPtr createVideo(std::string const& name, std::string const& pathname, unsigned duration);

	/**
	 * @brief Crée un film et l'ajoute au dictionnaire des médias
	 * @param name Nom du film
	 * @param pathname Chemin vers le fichier sur le disque
	 * @param duration Durée du film
	 * @param nb_chapters Nombre de chapitre (taille du tableau passé par chapters)
	 * @param chapters Table des chapitres (durée de chaque chapitre)
	 * @return Pointeur sur le film (std::shared_ptr)
	 */
	MediaPtr createFilm(std::string const& name, std::string const& pathname, unsigned duration, unsigned nb_chapters, unsigned const * const chapters);
	
	/**
	 * @brief Crée un groupe et l'ajoute au dictionnaire des groupes
	 * @param name Nom du groupe
	 * @return Pointeur sur le groupe (std::shared_ptr)
	 */
	GroupePtr createGroup(std::string const& name);

	/**
	 * @brief Renvoie le média portant le nom name
	 * @param name nom du média à chercher
	 * @return Un pointeur vers le media s'il existe et nullptr sinon
	 */
	MediaPtr getMedia(std::string const& name) const;

	/**
	 * @brief Renvoie le groupe portant le nom name
	 * @param name nom du groupe à chercher
	 * @return Un pointeur vers le groupe s'il existe et nullptr sinon
	 */
	GroupePtr getGroupe(std::string const& name) const;

	/**
	 * @brief Affiche le media et/ou le groupe portant le nom name sur la sortie standard
	 * @param name nom du media/groupe à afficher
	 * @throw std::runtime_error si name ne désigne ni un groupe ni un média
	 */
	void print(std::string const& name) const;

	/**
	 * @brief Joue le media portant le nom name
	 * @param name nom du media à jouer
	 * @throw std::runtime_error si name ne désigne pas un média
	 */
	void play(std::string const& name) const;

	/**
	 * @brief Supprime le media portant le nom name et le supprime de tous les groupes auquel il appartient
	 * @param name nom du media à supprimer
	 * @throw std::runtime_error si name ne désigne pas un média
	 */
	void removeMedia(std::string const& name);

	/**
	 * @brief Supprime le groupe portant le nom name
	 * @param name nom du groupe à supprimer
	 * @throw std::runtime_error si name ne désigne pas un groupe
	 */
	void removeGroup(std::string const& name);

	/**
	 * @brief Répond au requêtes formatées.
	 * Requêtes possible :
	 * - GETALLMEDIA : Renvoie tous les médias de la BDD
	 * - GETALLGROUPS : Renvoie tous les groupes de la BDD
	 * - GETGROUP : Renvoie tous les médias d'un groupe
	 * - GETMEDIA : Renvoie les infos d'un média
	 * - REMOVEMEDIA : Supprime un media
	 * - REMOVEGROUP : Supprime un groupe
	 * - ADDMEDIATOGROUP : Ajoute un media à un groupe
	 * - REMOVEMEDIAFROMGROUP : Supprime un média d'un groupe
	 * - ADDPHOTO : Crée un photo
	 * - ADDVIDEO : Crée une vidéo
	 * - ADDFILM : Crée un film
	 * - ADDGROUP : Crée un groupe
	 * - PLAY : Joue un média
	 * @param req Requête
	 * @param res Réponse
	 * @return false ssi la connexion doit être fermée.
	 */
	bool request(std::string const& req, std::string& res);

	friend std::ostream& operator<<(std::ostream& os, MediaManager const& mmgr);
	friend std::istream& operator>>(std::istream& is, MediaManager& mmgr);

private:

	std::map<std::string, std::shared_ptr<Groupe>> groups;
	std::map<std::string, MediaPtr> medias;

	/**
	 * @brief Crée un média vide correspondant au nom de la classe passée en paramêtre
	 * @param classname Nom de la classe à créer (Photo, Video ou Film)
	 * @return Pointeur vers le média créé
	 * @throw std::invalid_argument si classname n'est pas reconnu
	 */
	static MediaPtr createFromClassName(std::string const& classname);

};

#endif