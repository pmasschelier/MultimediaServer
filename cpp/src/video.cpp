#include "video.hpp"

void Video::print(std::ostream & os) const {
	Media::print(os);
	os << "durée : " << duration << ';';	
}

void Video::serialize(std::ostream & os) const {
	Media::serialize(os);
	os << duration << std::endl;
}

void Video::deserialize(std::istream& is) {
	Media::deserialize(is);
	is >> duration;
	std::string end;
	getline(is, end);
}

void Video::play() const {
	std::string command("mpv " + getPathname() + " &");
	system(command.data());
}