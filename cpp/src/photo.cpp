#include "photo.hpp"

void Photo::print(std::ostream & os) const {
	Media::print(os);
	os << "largeur : " << width << ", hauteur : " << height << ';';
}

void Photo::serialize(std::ostream & os) const {
	Media::serialize(os);
	os << width << ' ' << height << std::endl;
}

void Photo::deserialize(std::istream& is) {
	Media::deserialize(is);
	is >> width >> height;
	std::string end;
	getline(is, end);
}

void Photo::play() const {
	std::string command("imagej " + getPathname()  + " &");
	system(command.data());
}