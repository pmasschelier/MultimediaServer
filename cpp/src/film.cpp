#include "film.hpp"
#include <cstring>
#include <sstream>
#include <vector>

static unsigned* copyChapters(unsigned nb_chapters, unsigned const * const chapters)
{
	unsigned* ret = nullptr;
	if(chapters != nullptr && nb_chapters != 0) {
		ret = new unsigned[nb_chapters];
		memcpy(ret, chapters, nb_chapters * sizeof(int));
	}
	return ret;
}

Film::Film(std::string const& name, std::string const& pathname, unsigned duration, unsigned nb_chapters, unsigned const * const chapters) :
    Video(name, pathname, duration)
{
    setChapters(nb_chapters, chapters);
}

Film::Film(Film const& other) : Video(other)
{
    nb_chapters = other.getNbChapters();
    m_chapters = copyChapters(nb_chapters, other.getChapters());
}

Film& Film::operator=(Film const& other) {
    Video::operator=(*this);
    nb_chapters = other.getNbChapters();
    m_chapters = copyChapters(nb_chapters, other.getChapters());
    return *this;
}

Film::~Film() {
    delete[] m_chapters;
}

void Film::setChapters(unsigned nb_chapters, unsigned const * const chapters)
{
    delete[] m_chapters;
    this->nb_chapters = nb_chapters;
    m_chapters = copyChapters(nb_chapters, chapters);
}

unsigned const * Film::getChapters() const
{
    return m_chapters;
}

void Film::print(std::ostream & os) const
{
    Video::print(os);
    for(unsigned i = 0; i < nb_chapters; i++)
        os << "chapitre " << i << " : durée = " << m_chapters[i] << ';';
}

void Film::serialize(std::ostream & os) const {
	Video::serialize(os);
	for(unsigned i = 0; i < nb_chapters; i++)
		os << m_chapters[i] << ' ';
	os << std::endl;
}

void Film::deserialize(std::istream& is) {
	Video::deserialize(is);
	std::string line;
	getline(is, line);
	std::istringstream sstr(line);
	std::vector<unsigned> chapters;
	while(sstr) {
		unsigned d{};
		sstr >> d;
		if(sstr)
			chapters.push_back(d);
	}
	setChapters(chapters.size(), &chapters[0]);
}