#include "groupe.hpp"

Groupe::Groupe(std::string const& name) {
	if(!checkName(name))
		throw std::invalid_argument("Un nom de groupe ne peut contenir que des caractère alphanumérique et de la ponctuation.");
	this->name = name;
}

void Groupe::print(std::ostream & os) const {
	bool first = true;
	for(auto it = begin(); it != end(); it ++) {
		if(!first) os << ';';
		first = false;
		os << (*it)->getName();
	}
}

std::ostream& operator<<(std::ostream & os, Groupe const & group) {
	group.print(os);
	return os;
}