#include "mediamanager.hpp"

MediaPtr MediaManager::createPhoto(std::string const& name, std::string const& pathname, unsigned width, unsigned height) {
	if(medias.find(name) != medias.end())
		throw std::runtime_error(name + " existe déjà dans la table des médias.");
	return medias[name] = std::make_shared<Photo>(name, pathname, width, height);
}

MediaPtr MediaManager::createVideo(std::string const& name, std::string const& pathname, unsigned duration){
	if(medias.find(name) != medias.end())
		throw std::runtime_error(name + " existe déjà dans la table des médias.");
	return medias[name] = std::make_shared<Video>(name, pathname, duration);
}

MediaPtr MediaManager::createFilm(std::string const& name, std::string const& pathname, unsigned duration, unsigned nb_chapters, unsigned const * const chapters) {
	if(medias.find(name) != medias.end())
		throw std::runtime_error(name + " existe déjà dans la table des médias.");
	return medias[name] = std::make_shared<Film>(name, pathname, duration, nb_chapters, chapters);
}

GroupePtr MediaManager::createGroup(std::string const& name) {
	if(groups.find(name) != groups.end())
		throw std::runtime_error(name + " existe déjà dans la table des groupes.");
	return groups[name] = std::make_shared<Groupe>(name);
}

MediaPtr MediaManager::getMedia(std::string const& name) const {
		auto it = medias.find(name);
		if(it != medias.end())
			return it->second;
		std::cerr << name << "n'est pas un média." << std::endl;
		return nullptr;
	}

GroupePtr MediaManager::getGroupe(std::string const& name) const {
	auto it = groups.find(name);
	if(it != groups.end())
		return it->second;
	std::cerr << name << "n'est pas un groupe." << std::endl;
	return nullptr;
}

void MediaManager::print(std::string const& name) const {
	auto it1 = medias.find(name);
	auto it2 = groups.find(name);
	if(it1 != medias.end())
		it1->second->print(std::cout);
	if(it2 != groups.end())
		it2->second->print(std::cout);
	if(it1 == medias.end() && it2 == groups.end())
		throw std::runtime_error(name + " n'est ni un groupe ni un média.");
}

void MediaManager::play(std::string const& name) const {
	auto it = medias.find(name);
	if(it != medias.end())
		it->second->play();
	if(it == medias.end())
		throw std::runtime_error(name + " n'est pas un média.");
}

void MediaManager::removeMedia(std::string const& name) {
	if(medias.erase(name) == 0)
		throw std::runtime_error(name + " n'est pas présent dans la table des médias.");
	for(auto& g : groups) {
		for(auto it = g.second->begin(); it != g.second->end(); it ++) {
			if((*it)->getName() == name)
				it = g.second->erase(it);
		}
	}
}

void MediaManager::removeGroup(std::string const& name) {
	if(groups.erase(name) == 0)
		throw std::runtime_error(name + " n'est pas présent dans la table des groupes.");
}

MediaPtr MediaManager::createFromClassName(std::string const& classname) {
	if(classname == "Video")
		return std::make_shared<Video>();
	if(classname == "Film")
		return std::make_shared<Film>();
	if(classname == "Photo")
		return std::make_shared<Photo>();
	throw std::invalid_argument(classname + "n'est pas un type de Media");
}

std::ostream& operator<<(std::ostream& os, MediaManager const& mmgr) {
	for(auto& media : mmgr.medias) {
		os << media.second->getClassName() << std::endl;
		os << *media.second;
		if(os.fail())
			throw std::runtime_error("Erreur lors de l'écriture de " + media.first);
	}
	for(auto& groupe : mmgr.groups) {
		os << "GROUP " << groupe.first << std::endl;
		for(auto& media : *groupe.second)
			os << media->getName() << ';';
		os << std::endl;
		if(os.fail())
			throw std::runtime_error("Erreur lors de l'écriture de " + groupe.first);
	}
	return os;
}

std::istream& operator>>(std::istream& is, MediaManager& mmgr) {
	mmgr.medias.clear();
	while(is) {
		std::string line;
		getline(is, line);
		if(line.substr(0, 5) == "GROUP") {
			GroupePtr ptr = mmgr.createGroup(line.substr(5));
			getline(is, line);
			std::istringstream members(line);
			std::string m;
			getline(members, m, ';');
			ptr->push_back(mmgr.getMedia(m));
		}
		else {
			MediaPtr ptr(MediaManager::createFromClassName(line));
			is >> *ptr;
			mmgr.medias[ptr->getName()] = ptr;
		}
	}
	return is;
}