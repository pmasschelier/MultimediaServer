#include <iostream>
#include <fstream>
#include <array>
#include "mediamanager.hpp"
#include "tcpserver.hpp"
#include <cassert>

int PORT = 3331;

bool saveMedias(std::string const& filename, MediaManager const& mmgr) {
	std::ofstream file(filename);
	if(!file) {
		std::cerr << "Impossible d'ouvrir en lecture le fichier : " << filename << std::endl;
		return false;
	}
	try {
		file << mmgr;
	}
	catch(std::exception& e) {
		std::cerr << e.what() << std::endl;
		return false;
	}
	return true;
}

bool readMedias(std::string const& filename, MediaManager& mmgr) {
	std::ifstream file(filename);
	if(!file) {
		std::cerr << "Impossible d'ouvrir en écriture le fichier : " << filename << std::endl;
		return false;
	}
	try {
		file >> mmgr;
	}
	catch(std::exception& e) {
		std::cerr << e.what() << std::endl;
		return false;
	}
	return true;
}

int main(int argc, char* argv[])
{
	for(int i = 1; i < argc; i++) {
		std::string arg(argv[i]);
		std::string usage("USAGE : ./media -p 1234\n");
		if(arg == "-p") {
			if(i < argc - 1) {
				try{
					arg = argv[++i];
					int p = std::stoi(arg);
					PORT = p;
				}
				catch(std::exception const& e) {
					std::cerr << usage;
				}
			}
			else
				std::cerr << usage;
		}
	}

	MediaManager mmg;
	GroupePtr group1 = mmg.createGroup("Super medias !");
	group1->push_back(mmg.createVideo("Le roi lion", "/home/tintin/Videos/leroilion.mkv", 220));
	group1->push_back(mmg.createPhoto("Un beau lion", "/home/tintin/Pictures/lion.jpg", 1080, 800));
	group1->push_back(mmg.createVideo("Dune", "/home/tintin/Video/dune.mkv", 345));
    unsigned chapters[] = {1, 2, 3, 4, 5, 6, 7};
	group1->push_back(mmg.createFilm("Star Wars", "/home/tintin/Video/starwars.mp4", 789, 7, chapters));

	std::cout << *group1 << std::endl;

	GroupePtr group2 = mmg.createGroup("Medias pas ouf");
	group2->push_back(mmg.createVideo("Gad El Maleh sketch", "/home/tintin/Video/gadelmaleh", 430));
	group2->push_back(mmg.getMedia("Star Wars"));

	std::cout << *group2 << std::endl;

	mmg.removeMedia("Dune");

	std::cout << *group1 << std::endl;
	std::cout << *group2 << std::endl;

	assert(mmg.getGroupe("Medias pas ouf") != nullptr);

	saveMedias("medias.dat", mmg);
	MediaManager mmg2;
	readMedias("medias.dat", mmg2);
	std::cout << "RELOADED" << std::endl;
	mmg2.print("Star Wars");
	std::cout << std::endl;

	TCPServer server([&mmg](std::string const& req, std::string& res) { return mmg.request(req, res); });

	// lance la boucle infinie du serveur
	std::cout << "Starting Server on port " << PORT << std::endl;

	int status = server.run(PORT);

	// en cas d'erreur
	if (status < 0) {
		std::cerr << "Could not start Server on port " << PORT << std::endl;
		return EXIT_FAILURE;
	}

	
    return EXIT_SUCCESS;
}
