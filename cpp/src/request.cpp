#include "mediamanager.hpp"

bool MediaManager::request(std::string const& req, std::string& res)
{
	std::istringstream istr(req);
	std::ostringstream ostr;
	std::string command, arg;

	istr >> command;

	char space;
	istr.get(space);
	
	getline(istr, arg, ';');
	
	if(command == "GETALLMEDIA") {
		bool first = true;
		for(auto& m : medias) {
			if(!first) ostr << ';';
			first = false;
			ostr << m.first;
		}
	}
	else if (command == "GETALLGROUPS") {
		bool first = true;
		for(auto& g : groups) {
			if(!first) ostr << ';';
			first = false;
			ostr << g.first;
		}
	}
	else if(command == "GETGROUP") {
		GroupePtr g = getGroupe(arg);
		if(g != nullptr)
			g->print(ostr);
		else
			ostr << "NOT FOUND";
	}

	else if(command == "GETMEDIA") {
		MediaPtr m = getMedia(arg);
		if(m != nullptr)
			m->print(ostr);
		else
			ostr << "NOT FOUND";
	}
	else if(command == "REMOVEMEDIA") {
		try {
			removeMedia(arg);
			ostr << "SUCCESS";
		}
		catch(std::runtime_error const& e) {
			ostr << "NOT FOUND";
		}
	}
	else if(command == "REMOVEGROUP") {
		try {
			removeGroup(arg);
			ostr << "SUCCESS";
		}
		catch(std::runtime_error const& e) {
			ostr << "NOT FOUND";
		}
	}
	else if(command == "ADDMEDIATOGROUP") {
		try {
			GroupePtr g = getGroupe(arg);
			getline(istr, arg, ';');
			MediaPtr m = getMedia(arg);
			if(std::find(g->begin(), g->end(), m) != g->end())
				throw std::runtime_error("Media already in playlist");
			g->push_back(getMedia(arg));
		}
		catch(std::runtime_error const& e) {
			ostr << "NOT FOUND";
		}
	}
	else if(command == "REMOVEMEDIAFROMGROUP") {
		try {
			GroupePtr g = getGroupe(arg);
			getline(istr, arg, ';');
			g->remove(getMedia(arg));
		}
		catch(std::runtime_error const& e) {
			ostr << "NOT FOUND";
		}
	}
	else if(command == "ADDPHOTO") {
		std::string location;
		int sizeX{}, sizeY{};
		getline(istr, location, ';');
		istr >> sizeX >> sizeY;
		if(istr.fail()) {
			ostr << "INCORRECT FORMAT";
		}
		try {
			createPhoto(arg, location, sizeX, sizeY);
		} catch(std::runtime_error const& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	else if(command == "ADDVIDEO") {
		std::string location;
		int duration{};
		getline(istr, location, ';');
		istr >> duration;
		if(istr.fail()) {
			ostr << "INCORRECT FORMAT";
		}
		try {
			createVideo(arg, location, duration);
		}
		catch(std::runtime_error const& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	else if(command == "ADDFILM") {
		std::string location;
		int duration, chapter_duration;
		std::vector<unsigned> durations;
		getline(istr, location, ';');
		istr >> duration;
		while(istr) {
			istr >> chapter_duration;
			durations.push_back(chapter_duration);
		}
		if(istr.fail()) {
			ostr << "INCORRECT FORMAT";
		}
		try {
			createFilm(arg, location, duration, durations.size(), &durations[0]);
		} catch(std::runtime_error const& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	else if(command == "ADDGROUP") {
		try {
			createGroup(arg);
		}
		catch(std::runtime_error const& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	else if(command == "PLAY") {
		MediaPtr m = getMedia(arg);
		if(m != nullptr)
			m->play();
		else
			ostr << "NOT FOUND";
	}

	else {
		ostr << "COMMAND UNKNOWN";
	}
	res = ostr.str();

	std::cout << req << std::endl;
	std::cout << res << std::endl;
	return true;
}