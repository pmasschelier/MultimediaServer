#include "media.hpp"

Media::Media(std::string const& name, std::string const& pathname) {
	setName(name);
	setPathname(pathname);
}

void Media::setName(std::string const& name) {
	if(!checkName(name))
		throw std::invalid_argument("Un nom de media ne peut contenir que des caractère alphanumérique et de la ponctuation.");
	this->name = name;
}

void Media::setPathname(std::string const& pathname) {
	if(!checkName(pathname))
		throw std::invalid_argument("Un chemin ne peut contenir que des caractère alphanumérique et de la ponctuation.");
	this->pathname = pathname;
}

void Media::print(std::ostream& os) const
{
    os << "pathname : " << pathname << ';';
    os << "name : " << name << ';';
}

void Media::serialize(std::ostream& os) const
{
    os << pathname << std::endl;
    os << name << std::endl;
}

void Media::deserialize(std::istream& is) {
	getline(is, pathname);
	getline(is, name);
}

std::ostream& operator<<(std::ostream & os, Media const & media) {
    media.serialize(os);
    return os;
}

std::istream& operator>>(std::istream & is, Media& media) {
    media.deserialize(is);
    return is;
}